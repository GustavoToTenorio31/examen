package com.example.fechas.ViewModels;
import java.util.List;
public class PeticionFechas {
    public String fecha;
    public List<PeticionFechas>importante;
    public PeticionFechas(){}

    public PeticionFechas(String fecha, List<PeticionFechas>detalle){
        this.fecha=fecha;
        this.importante=detalle;
    }
    public String getFecha()
    {return fecha;}
    public void setFecha(String fecha)
    {this.importante=importante;}
    public List<PeticionFechas>getDetalle()
    {return importante;}
    public void setImportante(List<PeticionFechas>detalle)
    {this.importante=importante; }

}
