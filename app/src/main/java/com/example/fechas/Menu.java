package com.example.fechas;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.example.fechas.Api.Api;
import com.example.fechas.Api.Servicios.ServicioPeticion;
import com.example.fechas.ViewModels.PeticionFechas;
import com.example.fechas.ViewModels.PeticionLogin;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu extends AppCompatActivity {
    String url="https://notificacionupt.andocodeando.net/api/todasNot";
    private String APITOKEN="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        WebView link=(WebView)findViewById(R.id.mostrar);
        link.setWebViewClient(new MyWebViewClient());
        WebSettings config=link.getSettings();
        config.setJavaScriptEnabled(true);
        link.loadUrl(url);



        Button cerr=(Button)findViewById(R.id.closeS);
        cerr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(Menu.this,MainActivity.class);
                startActivity(i);
                finish();

            }
        });
        ServicioPeticion service = Api.getApi(Menu.this).create(ServicioPeticion.class);
        Fragment fecha;
        Fragment importante;
        Call<PeticionFechas> fechasCall = service.getNoticias(fecha.getText().toString(), importante.getText().toString());
        fechasCall.enqueue(new Callback<PeticionFechas>() {
            @Override
            public void onResponse(Call<PeticionFechas> call, Response<PeticionFechas> response) {
                PeticionFechas peticion = response.body();
                if (peticion.fecha == "true") {
                    APITOKEN = peticion.fecha;
                    Toast.makeText(Menu.this, "Fechas importantes", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(Menu.this, Menu.class));
                }
            }

            @Override
            public void onFailure(Call<PeticionFechas> call, Throwable t) {
                Toast.makeText(Menu.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    private class MyWebViewClient extends WebViewClient{
        public  boolean shouldOverrideUrlLoading(WebView view,String url){
            view.loadUrl(url);
            return true;

        }

    }


}
